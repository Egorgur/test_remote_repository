from PyQt5 import QtWidgets
import calculate

from template import Ui_Form


class MainWindow(Ui_Form):
    def __init__(self, Form):
        super().__init__()
        self.setupUi(Form)
        self.list_of_button = [
            self.x11,
            self.x12,
            self.x21,
            self.x22,
            self.y11,
            self.y12,
            self.y21,
            self.y22,
        ]
        self.Calculate_distance.clicked.connect(self.on_click)

    def on_click(self):
        dict_position = [i.value() for i in self.list_of_button]
        print(dict_position)

        self.distance_bet_cent.setText(str(calculate.calculate_distance_between_center(*dict_position)))
        self.distance_betw_edges.setText(str(calculate.get_distance_between_squares_point(*dict_position)))

if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = MainWindow(Form)
    Form.show()
    sys.exit(app.exec_())


    # def __init__(self, scene, parent=None):
    #     super(ScalableGraphicsView, self).__init__(scene, parent)
    #     self.setRenderHint(QPainter.Antialiasing, True)
    #     self.setRenderHint(QPainter.SmoothPixmapTransform, True)
    #     self.setRenderHint(QPainter.TextAntialiasing, True)
    #     self.setRenderHint(QPainter.HighQualityAntialiasing, True)
    #     self.setRenderHint(QPainter.NonCosmeticDefaultPen, True)
    #     self.setDragMode(QGraphicsView.ScrollHandDrag)
    #
    # def wheelEvent(self, event):
    #     factor = 1.2
    #     if event.angleDelta().y() < 0:
    #         factor = 1.0 / factor
    #     self.scale(factor, factor)