import math


def validate_data(x11, y11, x12, y12, x21, y21, x22, y22):
    """Независимо от переданных координат выводит координаты
    левого верхнего и правого нижнего"""

    return (
        min(x11, x12), max(y11, y12),
        max(x11, x12), min(y11, y12),
        min(x21, x22), max(y21, y22),
        max(x21, x22), min(y21, y22),
    )


def get_distance_between_point(x1, y1, x2, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def calculate_distance_between_center(x11, y11, x12, y12, x21, y21, x22, y22):
    """Дистанция между центрами прямоугольников"""
    x11, y11, x12, y12, x21, y21, x22, y22 = validate_data(x11, y11, x12, y12, x21, y21, x22, y22)

    def get_center_position(x1, y1, x2, y2):
        return (x1 + x2) / 2, (y1 + y2) / 2

    return round(get_distance_between_point(
        *get_center_position(x11, y11, x12, y12),
        *get_center_position(x21, y21, x22, y22),
    ), 2)  # округляем ответ до двух знаков после запятой


def get_distance_between_squares_point(x11, y11, x12, y12, x21, y21, x22, y22):
    """Сумма дистанций между левыми верхними углами и правыми нижними"""
    x11, y11, x12, y12, x21, y21, x22, y22 = validate_data(x11, y11, x12, y12, x21, y21, x22, y22)
    return round(get_distance_between_point(x11, y11, x21, y21) + get_distance_between_point(x12, y12, x22, y22), 2)


print(get_distance_between_squares_point(1, 2, 3, 0, 3, 4, 5, 2))
